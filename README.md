# EjemploStore

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# **PRUEBA DE ANGULARJS**

Nuestra pagina web muestra el manejo adecuado de angulasrJS mostrando datos obtenidos en una api usando bootstrap y manejo de componentes, los requerimientos para este proyecto son:

+ Maquetar la siguiente imagen
![Imagen](https://i.ibb.co/RQK5T93/0001.jpg")
+ La interfaz debe permitir seleccionar o escribir el numero de usuarios a buscar
+ Los usuarios deben ser visualizados en forma de CARDS

1. Para dar solucion a los requerimientos antes dichos lo principal es conocer el entorno de desarrollo de angularJS en el cual realizaremos nuestra pagina web:
![entorno](https://i.ibb.co/Wxm56vm/Captura-de-Pantalla-2020-05-13-a-la-s-2-54-45-p-m.png)

2. ahora que debemos obtener datos desde una api la cual es: [](https://randomuser.me/api/), creamos un service en el cual referenciaremos el link antes mencionado:

```swift
	import { Injectable } from '@angular/core';
	import { HttpClient } from '@angular/common/http';
	
	@Injectable({
	  providedIn: 'root'
	})
	export class UserService {
	
	  constructor(protected http: HttpClient) { }
	
	  getUsers() {
	    return this.http.get('https://randomuser.me/api');
	  }
	}
```
en este podemos ver que se implementa el HyperText Transfer Protocol importando la clase HttpClient.

3. mediante el comando ng g c *<nombrecomponente>*  creamos los componentes que deseemos
```swift
Adsegs-MacBook-Pro:ejemplo-store AdalbertoS$ ng g c card
CREATE src/app/card/card.component.sass (0 bytes)
CREATE src/app/card/card.component.html (19 bytes)
CREATE src/app/card/card.component.spec.ts (614 bytes)
CREATE src/app/card/card.component.ts (268 bytes)
UPDATE src/app/app.module.ts (754 bytes)
```
```swift
Adsegs-MacBook-Pro:ejemplo-store AdalbertoS$ ng g c search
CREATE src/app/search/search.component.sass (0 bytes)
CREATE src/app/search/search.component.html (21 bytes)
CREATE src/app/search/search.component.spec.ts (628 bytes)
CREATE src/app/search/search.component.ts (276 bytes)
UPDATE src/app/app.module.ts (786 bytes)
```

4. una vez haya terminado de crear los componentes nos creará carpetas para cada componente, nosotros ocuparemos primero el archivo *card.component.html* en el cual escribiremos lo siguiente
```swift
<div class="container">
    <div class="row">
      <div *ngFor="let dummy of ' '.repeat(prueba).split(''), let x = index">
        <div class="col-12">
          <div class="datos" *ngFor='let user of users'>
            <div class="card mt-4" role="button" tabindex="0">
              <div class="card-body">
                <div class="text-center">
                  <img class="rounded-circle" [src]="user.picture.medium">
                <h5>@{{user.login.username}}</h5>
                </div>
                <small><h6>{{ user.name.title }}&nbsp;{{ user.name.first}}&nbsp;{{ user.name.last}}</h6>
                <p class="mb-1">{{ user.email }}</p>
                <p class="mb-1">{{ user.location.state }}&nbsp;#&nbsp;{{ user.location.postcode }}</p>
                <p class="mb-1">{{ user.location.city }},&nbsp;{{ user.location.state }}&nbsp;-&nbsp;{{ user.location.country }}</p>
                </small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
```

5. y en *card.component.ts* escribiremos el siguiente codigo:
```swift
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})

export class CardComponent implements OnInit {
  prueba : number = 0;
  users : Array<String> = [];

  constructor(protected userService: UserService) {
  }

  ngOnInit(): void {
  }
  
  getDatos(cantidad : number){
    this.userService.getUsers()
    .subscribe(
      (data) => { // Success
        this.users = data['results'];
      },
      (error) => {
        console.error(error);
      }
    );

    this.prueba = cantidad;
  }

}
```

6. En *search.component.html* escribiremos el siguiente codigo en donde al final llamaremos a card.component.ts con su selector *<app-card>* 

```swift
<div class="container mt-6">
    <h3 class="mt-4">Users</h3>
    <div class="input-group mt-6 pt-3">
      <input type="number" [(ngModel)]='inputS' (keyup)='getCard($event)' class="form-control col-md-3">
    </div>
    <hr>
    <app-card></app-card>
</div>
```
7. en *search.component.ts* escribiremos el siguiente:

```swift
import { Component, OnInit, ViewChild } from '@angular/core';
import { CardComponent } from '../card/card.component';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})

export class SearchComponent implements OnInit {

  @ViewChild(CardComponent) card: CardComponent;
  inputS : number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  getCard(event: any){
    this.card.getDatos(event.target.value);
  }
}
```

8. por ultimo en el index.html llamaremos a los componentes mediante el selector *<app-root>*

9. ahora probamos nuestra pagina web corriendo nuestro server angular con el comando ***ng serve*** en nuestra terminal y el resultado es el siguiente:
![img](https://i.ibb.co/qBpWMfJ/Captura-de-Pantalla-2020-05-13-a-la-s-3-08-50-p-m.png)


