import { Component, OnInit, ViewChild } from '@angular/core';
import { CardComponent } from '../card/card.component';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.styles.css']
})

export class SearchComponent implements OnInit {

  @ViewChild(CardComponent) card: CardComponent;
  inputS : number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  getCard(event: any){
    this.card.getDatos(event.target.value);
  }
}
