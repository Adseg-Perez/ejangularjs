import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.styles.css']
})

export class CardComponent implements OnInit {
  prueba : number = 0;
  users : Array<String> = [];

  constructor(protected userService: UserService) {
  }

  ngOnInit(): void {
  }
  
  getDatos(cantidad : number){
    this.userService.getUsers()
    .subscribe(
      (data) => { // Success
        this.users = data['results'];
      },
      (error) => {
        console.error(error);
      }
    );

    this.prueba = cantidad;
  }

}
